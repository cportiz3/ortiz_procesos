﻿using System;

namespace Ortiz_Examen
{
    class Program
    {
        static void Main(string[] args)
        {
            float numero1, numero2, resultado;
            Console.WriteLine("Ingrese el primer número: ");
            numero1 = float.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el segundo número: ");
            numero2 = float.Parse(Console.ReadLine());
            resultado = numero1 + numero2;
            Console.WriteLine("La suma es: " + resultado);
            Console.ReadLine();
        }
    }
}
